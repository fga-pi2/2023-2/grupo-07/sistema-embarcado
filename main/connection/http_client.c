#include "esp_http_client.h"
#include "esp_crt_bundle.h"
#include "esp_log.h"
#include "../include/helpers.h"
#include<stdio.h>

static const char *TAG = "HTTP_CLIENT";
char *ultimo_endpoint = NULL;
char **rfids_valids = NULL;

esp_err_t _http_event_handler(esp_http_client_event_t *evt) {
  printf("HTTP_EVENT_ON_DATA, len=%d, status=%d\n", evt->data_len, esp_http_client_get_status_code(evt->client));
    switch(evt->event_id) {
        case HTTP_EVENT_ERROR:
            ESP_LOGD(TAG, "HTTP_EVENT_ERROR");
            break;
        case HTTP_EVENT_ON_CONNECTED:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_CONNECTED");
            break;
        case HTTP_EVENT_HEADER_SENT:
            ESP_LOGD(TAG, "HTTP_EVENT_HEADER_SENT");
            break;
        case HTTP_EVENT_ON_HEADER:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_HEADER");
            printf("%.*s", evt->data_len, (char*)evt->data);
            break;
        case HTTP_EVENT_ON_DATA:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
            printf("HTTP_EVENT_ON_DATA, len=%d, status=%d\n", evt->data_len, esp_http_client_get_status_code(evt->client));
            if (!esp_http_client_is_chunked_response(evt->client)) {
                printf("%.*s\n", evt->data_len, (char*)evt->data);
                
                char result[256];
                findSubstring(ultimo_endpoint, "rfid-drones", result);

                if (result[0] != '\0') {
                    // set_rfids_valids((char*)evt->data);
                    char** rfids = convert_strings_in_array_the_str((char*)evt->data);
                    rfids_valids = rfids;
                };
            }
            break;
        case HTTP_EVENT_ON_FINISH:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_FINISH");
            break;
        case HTTP_EVENT_DISCONNECTED:
            ESP_LOGD(TAG, "HTTP_EVENT_DISCONNECTED");
            break;
        case HTTP_EVENT_REDIRECT:
            ESP_LOGD(TAG, "HTTP_EVENT_REDIRECT");
            break;
    }
    return ESP_OK;
}

void http_request(char* url, char* method, char* body) {
    ultimo_endpoint = url;

    // verificar qual o tipo de method
    if (strcmp(method, "GET") == 0) {
        esp_http_client_config_t config = {
            .url = url,
            .method = HTTP_METHOD_GET,
            .event_handler = _http_event_handler,
            .crt_bundle_attach = esp_crt_bundle_attach
        };
        esp_http_client_handle_t client = esp_http_client_init(&config);
        esp_err_t err = esp_http_client_perform(client);
        if (err == ESP_OK) {
            ESP_LOGI(TAG, "HTTP request sent url: %s", url);
        } else {
            ESP_LOGE(TAG, "HTTP request failed: %s", esp_err_to_name(err));
        }
        esp_http_client_cleanup(client);
    } else if (strcmp(method, "POST") == 0) {
        esp_http_client_config_t config = {
            .url = url,
            .method = HTTP_METHOD_POST,
            .event_handler = _http_event_handler,
            .user_data = body,
            .crt_bundle_attach = esp_crt_bundle_attach
        };
        esp_http_client_handle_t client = esp_http_client_init(&config);
        // Set headers
        esp_http_client_set_header(client, "Content-Type", "application/json");
        // Set post field
        esp_http_client_set_post_field(client, body, strlen(body));
        // Send request
        esp_err_t err = esp_http_client_perform(client);
        if (err == ESP_OK) {
            ESP_LOGI(TAG, "HTTP request sent body: %s", body);
        } else {
            ESP_LOGE(TAG, "HTTP request failed: %s", esp_err_to_name(err));
        }
        esp_http_client_cleanup(client);
    } else if (strcmp(method, "PUT") == 0) {
        esp_http_client_config_t config = {
            .url = url,
            .method = HTTP_METHOD_PUT,
            .event_handler = _http_event_handler,
            .user_data = body,
            .crt_bundle_attach = esp_crt_bundle_attach
        };
        esp_http_client_handle_t client = esp_http_client_init(&config);
        esp_http_client_set_header(client, "Content-Type", "application/json");
        esp_http_client_set_post_field(client, body, strlen(body));
        esp_err_t err = esp_http_client_perform(client);
        if (err == ESP_OK) {
            ESP_LOGI(TAG, "HTTP request sent body: %s", body);
        } else {
            ESP_LOGE(TAG, "HTTP request failed: %s", esp_err_to_name(err));
        }
        esp_http_client_cleanup(client);
    } else if (strcmp(method, "DELETE") == 0) {
        esp_http_client_config_t config = {
            .url = url,
            .method = HTTP_METHOD_DELETE,
            .event_handler = _http_event_handler,
            .crt_bundle_attach = esp_crt_bundle_attach
        };
        esp_http_client_handle_t client = esp_http_client_init(&config);
        esp_err_t err = esp_http_client_perform(client);
        if (err == ESP_OK) {
            ESP_LOGI(TAG, "HTTP request sent url: %s", url);
        } else {
            ESP_LOGE(TAG, "HTTP request failed: %s", esp_err_to_name(err));
        }
        esp_http_client_cleanup(client);
    } else {
        ESP_LOGE(TAG, "HTTP request failed: %s", "Method not supported");
    }
}

char** get_rfids() {
    return rfids_valids;
}