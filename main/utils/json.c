#include<cJSON.h>

#include "../include/json.h"

extern char *mac_address;

json parse_json(char const *json_str)
{
  cJSON *response = cJSON_Parse(json_str);
  json __json;

  __json.method = cJSON_GetObjectItem(response, "method")->valuestring;
  __json.params = cJSON_GetObjectItem(response, "params")->valuestring;

  return __json;
}