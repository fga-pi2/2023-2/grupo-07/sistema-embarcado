#include <string.h>
#include <stdlib.h>
#include <stdio.h>

char** convert_strings_in_array_the_str(char* str) {
    const char delimiters[] = "[],\"";
    
    char **array;
    
    array = (char **)malloc(10 * sizeof(char *));
    
    char *token = strtok(str, delimiters);
    int i = 0;
    
    while (token != NULL) {
        array[i] = (char *)malloc(strlen(token) + 1);
        
        strcpy(array[i], token);
        token = strtok(NULL, delimiters);
        
        i++;
    }

    // array[i] = "***";
    array[i] = NULL;
    return array;
}

void findSubstring(const char *url, const char *substring, char *result) {
    while (*url) {
        // Verificar se encontramos a substring
        int i, j;
        for (i = 0, j = 0; substring[j] && url[i] == substring[j]; ++i, ++j);

        // Se encontrarmos a substring, copiar para o resultado
        if (!substring[j]) {
            int k;
            for (k = 0; substring[k]; ++k) {
                result[k] = url[i - k - 1];
            }
            result[k] = '\0';
            return;
        }

        ++url;
    }

    // Se não encontrarmos a substring, result será uma string vazia
    result[0] = '\0';
}

int verify_rfid(char* rfid, char** rfids_valids) {
    int i = 0;
    printf("Verifying RFID...\n");
    while (rfids_valids[i] != NULL) {
        printf("RFID: %s\n", rfids_valids[i]);
        if (strcmp(rfid, rfids_valids[i]) == 0) {
            return 1;
        }
        i++;
    }
    return 0;
}

// Função de comparação para qsort
int compararFloats(const void *a, const void *b) {
    return (*(float *)a > *(float *)b) - (*(float *)a < *(float *)b);
}

float calcularModa(float array[], int tamanho) {
    // Verifica se o array é válido
    if (array == NULL || tamanho <= 0) {
        printf("Array inválido.\n");
        return 0.0; // Valor padrão se o array for inválido
    }

    // Ordena o array para facilitar a contagem
    qsort(array, tamanho, sizeof(float), compararFloats);

    float moda = array[0]; // Valor inicial da moda
    int frequenciaAtual = 1; // Frequência atual
    int maxFrequencia = 1; // Frequência máxima

    // Percorre o array a partir do segundo elemento
    for (int i = 1; i < tamanho; i++) {
        // Se o elemento atual é igual ao anterior, incrementa a frequência
        if (array[i] == array[i - 1]) {
            frequenciaAtual++;
        } else {
            // Se a frequência atual é maior que a máxima registrada, atualiza a moda
            if (frequenciaAtual > maxFrequencia) {
                maxFrequencia = frequenciaAtual;
                moda = array[i - 1];
            }

            // Reinicia a contagem para o novo elemento
            frequenciaAtual = 1;
        }
    }

    // Verifica a frequência do último elemento
    if (frequenciaAtual > maxFrequencia) {
        moda = array[tamanho - 1];
    }

    return moda;
}


