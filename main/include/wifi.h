#ifndef WIFI_H
#define WIFI_H


typedef struct {
    int isCarLocked;
    int isDoorOpen;
    int radioVolume;
} NVSData;


void wifi_start();

NVSData read_nvs();
void write_nvs(const NVSData* data);

#endif
