#ifndef REDIS_H
#define REDIS_H

#include <stdint.h>

// Inicializa a conexão com o Redis
void redis_init();

// Registra um valor no Redis
void redis_set(const char *key, const char *value);

// Obtém um valor do Redis
const char *redis_get(const char *key);

#endif
