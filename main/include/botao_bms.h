// motor.h

#ifndef BOTAO_BMS_H
#define BOTAO_BMS_H

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/semphr.h>
#include <esp_system.h>
#include <driver/gpio.h>

#define PIN_CARREGAR  GPIO_NUM_19
#define PIN_PARAR     GPIO_NUM_23

extern int is_charging;
extern int is_baterry_connected;
extern SemaphoreHandle_t mutex;

void setup(void);
void ativa_modo_carregamento(void);
void para_carregamento(void);

#endif
