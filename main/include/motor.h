// motor.h

#ifndef MOTOR_H
#define MOTOR_H

#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_system.h>
#include <driver/gpio.h>

// Definição dos pinos do motor de passo
#define PIN_STEP  GPIO_NUM_12
#define PIN_DIR   GPIO_NUM_14
#define PIN_SLEEP GPIO_NUM_13
#define PIN_ENABLE GPIO_NUM_27

extern int velocidade;
extern int rpm;
extern bool is_sleep;
extern bool is_opened;

// Função para girar o motor de passo
void motorPasso(int numPassos);

// Função principal para controle do motor
void motor_task(void);

void abre_tampa(void);
void fecha_tampa(void);

void modo_sleep(bool sleep);

#endif  // MOTOR_H
