#ifndef CURRENT_SENSOR_H
#define CURRENT_SENSOR_H

#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <driver/adc.h>
#include "../include/motor.h"  // Include motor.h if necessary

#define ACS712_PIN ADC1_CHANNEL_0
#define ACS712_SENSIBILIDADE 185 // mV/A 

void current_sensor_task(void *pvParameters);

#endif  // CURRENT_SENSOR_H
