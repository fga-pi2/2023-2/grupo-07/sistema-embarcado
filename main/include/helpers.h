
#ifndef HELPERS_H
#define HELPERS_H

char** convert_strings_in_array_the_str();
void findSubstring();
int verify_rfid();
float calcularModa(float array[], int tamanho);
int compararFloats(const void *a, const void *b);

#endif
