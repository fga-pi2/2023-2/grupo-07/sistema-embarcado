#ifndef MQTT_H
#define MQTT_H

void handle_received_data(const char *data);

void mqtt_start();

void mqtt_envia_mensagem(char * topico, char * mensagem);

// Function prototype for the MQTT message handler
void messageHandler(const char* topic, const char* payload, unsigned int length);

// Function prototype for registering the update variable callback
typedef void (*UpdateCarLockedFunction)(int value); // Adjust the data type of the variable accordingly
void registerUpdateCarLockedCallback(UpdateCarLockedFunction callback);

// Function prototype for registering the update variable callback
typedef void (*UpdateDoorOpenedFunction)(int value); // Adjust the data type of the variable accordingly
void registerUpdateDoorOpenedCallback(UpdateDoorOpenedFunction callback);

// Function prototype for registering the update variable callback
typedef void (*UpdateRadioOnFunction)(int value); // Adjust the data type of the variable accordingly
void registerUpdateRadioOnCallback(UpdateRadioOnFunction callback);

#endif