// botao_bms.h

#ifndef MEDIDOR_TENSAO_H
#define MEDIDOR_TENSAO_H

#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <driver/gpio.h>
#include <driver/adc.h>

#define PIN_BAT_DIVIDER 34 // Pino analógico conectado ao ponto de junção do divisor de tensão
#define PIN_TRANSISTOR GPIO_NUM_33

extern float battery_voltage_array[50];
extern int is_voltage_reading;
extern int voltage_reading_counter;

void setup_adc();
float read_battery_voltage();
void battery_measurement_task(void *pvParameters);

#endif
