#ifndef RFID_SENSOR_H
#define RFID_SENSOR_H

#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/timers.h>
#include <driver/uart.h>
#include <string.h>

#define RXD2 16
#define TXD2 17

#define TAG1 "3D00933868" // permitida
#define TAG2 "3D00936423" // não permitida
#define TAG3 "3D0093C7B0"  // permitida

static const uart_port_t uart_num = UART_NUM_2;

extern uint32_t millisReading;
extern uint8_t tagNumber;
extern char tagReading[20];

void setup_rfid();
void rfid_task(void *pvParameter);

#endif  // RFID_CONTROLLER_H
