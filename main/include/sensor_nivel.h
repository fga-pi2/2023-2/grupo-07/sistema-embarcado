// ultrasonic_sensor.h

#ifndef ULTRASONIC_SENSOR_H
#define ULTRASONIC_SENSOR_H

#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <driver/gpio.h>
#include <esp_timer.h>

// Definição dos pinos
#define TRIGGER_PIN 5
#define ECHO_PIN 18
#define LED_PIN 15

// Protótipo da função da tarefa ultrassônica
void ultrasonic_task(void *pvParameters);

#endif // ULTRASONIC_SENSOR_H
