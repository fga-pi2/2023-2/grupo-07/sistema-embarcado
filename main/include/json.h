#ifndef JSON_H
#define JSON_H

struct json {
  const char *method;
  const char *params;
} typedef json;

json parse_json(char const *json_str);

#endif