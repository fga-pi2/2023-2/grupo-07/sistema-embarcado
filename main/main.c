#include <stdio.h>
#include <string.h>
#include "nvs_flash.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_http_client.h"
#include "esp_log.h"
#include "freertos/semphr.h"

#include "include/sensor_nivel.h"
#include "include/motor.h"
#include "include/rfid_sensor.h"
#include "include/medidor_tensao.h"

#include "include/wifi.h"
#include "include/mqtt.h"
#include "include/http_client.h"

#define ID_BASE "3"

SemaphoreHandle_t conexaoWifiSemaphore;
SemaphoreHandle_t conexaoMQTTSemaphore;

void conectadoWifi(void * params) {
  while(true) {
    if(xSemaphoreTake(conexaoWifiSemaphore, portMAX_DELAY)) {
      // Processamento Internet
      mqtt_start();
    }
  }
}

void app_main(void) {
  // esp_err_t nvsFlash = nvs_flash_init();
  // if (nvsFlash == ESP_ERR_NVS_NO_FREE_PAGES || nvsFlash == ESP_ERR_NVS_NEW_VERSION_FOUND) {
  //   ESP_ERROR_CHECK(nvs_flash_erase());
  //   nvsFlash = nvs_flash_init();
  // }
  // ESP_ERROR_CHECK(nvsFlash);

  conexaoWifiSemaphore = xSemaphoreCreateBinary();
  // conexaoMQTTSemaphore = xSemaphoreCreateBinary();
  wifi_start();

  char url[255];
  sprintf(url, "https://monkfish-app-gyu5i.ondigitalocean.app/embarcados/rfid-drones/%s", ID_BASE);
  http_request(url, "GET", NULL);

  // xTaskCreate(&conectadoWifi,  "Conexão ao MQTT", 4096, NULL, 1, NULL);

  /**
   * Inicia tasks dos sensores
   */
  xTaskCreate(&ultrasonic_task, "ultrasonic_task", configMINIMAL_STACK_SIZE * 4, NULL, 5, NULL); // Ultrasonic
  xTaskCreate(&motor_task, "motor_task", configMINIMAL_STACK_SIZE * 4, NULL, 5, NULL); // Motor
  xTaskCreate(&rfid_task, "rfid_task", 4096, NULL, 10, NULL); // RFID
  xTaskCreate(&battery_measurement_task, "battery_task", 4096, NULL, 5, NULL); // Carregamento e Medidor de tensão

  // xTaskCreate(current_sensor_task, "current_sensor_task", configMINIMAL_STACK_SIZE * 4, NULL, 5, NULL); // Current

  // while(1) {
    // teste request
  // http_request("http://192.168.1.37:8080/history/por-rfid", "POST", "{\"rfid_drone\": \"67856756\", \"id_base\": 1}");
  //   vTaskDelay(20000/portTICK_PERIOD_MS);
  // }

  // while(1) {
  //   // teste request
  //   // http_request("http://192.168.1.37:8080/history/por-rfid", "POST", "{\"rfid_drone\": \"67856756\", \"id_base\": 1}");
  //   http_request("http://192.168.1.37:8080/embarcados/rfid-drones/1", "GET", NULL);
  //   vTaskDelay(5000/portTICK_PERIOD_MS);

  //   char **rfids_valids = get_rfids();

  //   // for (int i = 0; rfids_valids[i] != NULL; i++)
  //   //   printf("RFID: %s\n", rfids_valids[i]);
  //   printf("primeiro: %d",verify_rfid("11111111", rfids_valids));
  //   printf("segundo: %d",verify_rfid("67856756", rfids_valids));
  //   printf("terceiro: %d",verify_rfid("asdasdasdnjn\naiojsd", rfids_valids));
  //   printf("quarto: %d",verify_rfid("3D00936423", rfids_valids));

  //   vTaskDelay(5000/portTICK_PERIOD_MS);
  // }
}
