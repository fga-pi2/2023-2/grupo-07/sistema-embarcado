#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/timers.h>
#include <driver/uart.h>
#include <string.h>

#include "../include/rfid_sensor.h"
#include "../include/motor.h"
#include "../include/helpers.h"
#include "../include/http_client.h"
#include "../include/botao_bms.h"

#define RXD2 16
#define TXD2 17
#define ID_BASE "3"
// #define TAG1 "3D00933868" // permitida
// #define TAG2 "3D00936423" // não permitida
// #define TAG3 "3D0093C7B0"  // permitida

uint32_t millisReading = 0;
uint8_t tagNumber = 0;
char tagReading[20];

void setup_motor() {
    uart_config_t uart_config = {
        .baud_rate = 9600,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
    };

    uart_param_config(uart_num, &uart_config);
    uart_set_pin(uart_num, TXD2, RXD2, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    uart_driver_install(uart_num, 1024, 1024, 10, NULL, 0);

    printf("Serial Txd is on pin: %d\n", TXD2);
    printf("Serial Rxd is on pin: %d\n", RXD2);
}

void rfid_task(void *pvParameter) {
  setup_motor();
  while(1) {

    size_t availableBytes = 11;
    // uart_get_buffered_data_len(uart_num, &availableBytes);

    if (availableBytes > 0) {
        uart_read_bytes(uart_num, (uint8_t *)tagReading, availableBytes, portMAX_DELAY);
        tagReading[availableBytes] = '\0';

        for (size_t i = 0; i < availableBytes; ++i) {
            if (tagReading[i] == ' ') {
                tagReading[i] = '\0';  // Replace space with null terminator
                break;  // Exit the loop if a space is found
            }
        }

        printf("Tag RFID: %s\n", tagReading);

        if ((xTaskGetTickCount() * portTICK_PERIOD_MS - millisReading) > 5000) {
            // if (strcmp(tagReading, TAG1) == 0 || strcmp(tagReading, TAG3) == 0) {
            char **rfids_valids = get_rfids();

            if (verify_rfid(tagReading, rfids_valids)) {
                printf("Acesso permitido =D\n\n");
                vTaskDelay(5000 / portTICK_PERIOD_MS); // Delay to avoid high CPU usage
                abre_tampa();

                char body[255];
                sprintf(body, "{\"rfid_drone\": \"%s\", \"id_base\": %s}", tagReading, ID_BASE);
                http_request("https://monkfish-app-gyu5i.ondigitalocean.app/history/por-rfid", "POST", body);
            } else {
                printf("Tag não reconhecida!\n\n");
            }

            uart_flush(uart_num);
          
            millisReading = xTaskGetTickCount() * portTICK_PERIOD_MS;
        }
    }
     vTaskDelay(1000 / portTICK_PERIOD_MS); // Delay to avoid high CPU usage
  }
}

// void app_main() {
//     setup();

//     // Create a FreeRTOS task to mimic the loop function
//     xTaskCreate(loop, "loop_task", 4096, NULL, 10, NULL);
//     xTaskCreate(current_sensor_task, "current_sensor_task", configMINIMAL_STACK_SIZE * 4, NULL, 5, NULL);
//     xTaskCreate(motor_task, "motor_task", 4096, NULL, 10, NULL);
// }
