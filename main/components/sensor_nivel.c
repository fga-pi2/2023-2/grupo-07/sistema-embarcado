#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <driver/gpio.h>
#include <esp_timer.h>

#include "../include/sensor_nivel.h"

#include "../include/motor.h";
#include "../include/botao_bms.h";
#include "../include/rfid_sensor.h";

void ultrasonic_task(void *pvParameters) {
  gpio_reset_pin(TRIGGER_PIN);
  gpio_reset_pin(ECHO_PIN);
  gpio_reset_pin(LED_PIN);
  // definindo os modos do pinos
  gpio_set_direction(TRIGGER_PIN, GPIO_MODE_OUTPUT);
  gpio_set_direction(ECHO_PIN, GPIO_MODE_INPUT);
  gpio_set_direction(LED_PIN, GPIO_MODE_OUTPUT);

  while (1) {
    gpio_set_level(TRIGGER_PIN, 0);
    esp_rom_delay_us(2);
    //emitindo a onda do trigger
    gpio_set_level(TRIGGER_PIN, 1);
    esp_rom_delay_us(10);
    //setando ela em 0
    gpio_set_level(TRIGGER_PIN, 0);

    uint32_t start_time = esp_timer_get_time();
    uint32_t end_time = esp_timer_get_time();

    while (gpio_get_level(ECHO_PIN) == 0) {
      start_time = esp_timer_get_time();
    }
    while (gpio_get_level(ECHO_PIN) == 1) {
      end_time = esp_timer_get_time();
    }

    uint32_t duration = end_time - start_time;
    float distance = (duration * (343.0 / 1000000)) / 2;

    printf("Distancia: %.2f cm\n", distance*100);

    if ((distance*100) > 55 && is_opened) {

      is_baterry_connected = 0;

      // PUT drone AUSENTE
      
      gpio_set_level(LED_PIN, 1);
      fecha_tampa();
      para_carregamento();
      vTaskDelay(5000 / portTICK_PERIOD_MS);

    } else {
      if(is_opened && (distance*100) < 30){
        vTaskDelay(5000 / portTICK_PERIOD_MS);
        is_baterry_connected=1;

        // PUT drone CARREGANDO
      }
      gpio_set_level(LED_PIN, 0);
    }

    vTaskDelay(5000 / portTICK_PERIOD_MS);

  }
}