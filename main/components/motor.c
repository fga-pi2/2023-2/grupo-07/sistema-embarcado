
#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_system.h>
#include <driver/gpio.h>

#include "../include/motor.h"

#define PIN_CARREGAR  GPIO_NUM_19
#define PIN_PARAR     GPIO_NUM_23

// Definição das variáveis de controle do motor
int velocidade = 1500;  // Velocidade em microssegundos entre cada pulso (mais baixo = mais rápido) 1000= 1 segundo
//int sentido = 1;       // 1 para horário, -1 para anti-horário
int rpm = 5*600;          // Rotações por minuto desejadas 600= 1 volta completa

bool is_sleep = true; // Controla o sleep do motor
bool is_opened = false; // Verifica se porta está abrida
bool is_opening = false;
bool is_closing = false;

int passosPorMinuto;

int passosPorSegundo;

int numeroDeGiros;

int passosPorLoop;

/**
 * Sentido do motor:
 * 
 * Anti-horário: Abre Tampa
 * Horário: Fecha Tampa
 */

// Função para girar o motor de passo
void motorPasso(int numPassos) {
  for (int i = 0; i < numPassos; i++) {
    gpio_set_level(PIN_STEP, 1);
    esp_rom_delay_us(velocidade);
    gpio_set_level(PIN_STEP, 0);
    esp_rom_delay_us(velocidade);
  }
}

void abre_tampa(){
  if(!is_opened && !is_closing){
    is_sleep = false;
    vTaskDelay(1000 / portTICK_PERIOD_MS); // Delay to avoid high CPU usage
    printf("Abrindo tampa...\n\n");
    gpio_set_level(PIN_DIR, 0); // Sentido horário
    is_opening = true;
    motorPasso(passosPorLoop);
    vTaskDelay(5000 / portTICK_PERIOD_MS); // Delay to avoid high CPU usage
    
    is_opening = false;
    is_opened = true;
    is_sleep = true;
  }
}

void fecha_tampa(){
  if(is_opened && !is_opening){
    is_sleep = false;
    printf("Fechando tampa...\n\n");
    gpio_set_level(PIN_DIR, 1);
    is_closing = true;
    vTaskDelay(5000 / portTICK_PERIOD_MS);
    motorPasso(passosPorLoop);
    is_closing = false;
    is_opened = false;
    is_sleep = true;
  }
}

void modo_sleep(bool sleep){
  is_sleep = sleep;
}

void motor_task(void) {
  // Configuração dos pinos de controle do motor como saída
  gpio_reset_pin(PIN_STEP);
  gpio_set_direction(PIN_STEP, GPIO_MODE_OUTPUT);
  gpio_reset_pin(PIN_DIR);
  gpio_set_direction(PIN_DIR, GPIO_MODE_OUTPUT);

  gpio_reset_pin(PIN_SLEEP);
  gpio_reset_pin(PIN_ENABLE);

  gpio_set_direction(PIN_SLEEP, GPIO_MODE_OUTPUT);
  gpio_set_direction(PIN_ENABLE, GPIO_MODE_OUTPUT);

  // Cálculo do número de passos por minuto com base nas RPM
passosPorMinuto = 200 * rpm;  // Considerando motor Nema 17 com 200 passos por rotação

// Cálculo do número de passos por segundo
passosPorSegundo = passosPorMinuto / 60;


  while (1) {
    passosPorLoop = passosPorSegundo / portTICK_PERIOD_MS;  // portTICK_PERIOD_MS é a duração de cada tick (geralmente 1 ms em ESP-IDF)
    if(is_sleep) {
      // printf("Entrando em modo sleep...\n");
      gpio_set_level(PIN_SLEEP, 0);
      gpio_set_level(PIN_ENABLE, 1);
    } else {
      // printf("Saindo do modo sleep...\n");
      gpio_set_level(PIN_SLEEP, 1);
      gpio_set_level(PIN_ENABLE, 0);
    }
     vTaskDelay(100 / portTICK_PERIOD_MS); // Delay to avoid high CPU usage
  }
}

// void app_main() {
//   gpio_reset_pin(PIN_CARREGAR);

//   gpio_set_direction(PIN_CARREGAR, GPIO_MODE_OUTPUT);
//     // Create a FreeRTOS task to mimic the loop function
//     // xTaskCreate(motor_task, "motor_task", 4096, NULL, 10, NULL);

//     while(1) {
//       gpio_set_level(PIN_CARREGAR, 1);
//      vTaskDelay(2000 / portTICK_PERIOD_MS); // Delay to avoid high CPU usage
//       gpio_set_level(PIN_CARREGAR, 0);
//      vTaskDelay(5000 / portTICK_PERIOD_MS); // Delay to avoid high CPU usage
//     //   gpio_set_level(PIN_SLEEP, 0);
//     //  vTaskDelay(100 / portTICK_PERIOD_MS); // Delay to avoid high CPU usage

//     }
// }

// void app_main() {
//   gpio_reset_pin(PIN_STEP);
//   gpio_set_direction(PIN_STEP, GPIO_MODE_OUTPUT);
//   gpio_reset_pin(PIN_DIR);
//   gpio_set_direction(PIN_DIR, GPIO_MODE_OUTPUT);

//   gpio_reset_pin(PIN_SLEEP);
//   gpio_reset_pin(PIN_ENABLE);

//   gpio_set_direction(PIN_SLEEP, GPIO_MODE_OUTPUT);
//   gpio_set_direction(PIN_ENABLE, GPIO_MODE_OUTPUT);

//   // Cálculo do número de passos por minuto com base nas RPM
// passosPorMinuto = 200 * rpm;  // Considerando motor Nema 17 com 200 passos por rotação

// // Cálculo do número de passos por segundo
// passosPorSegundo = passosPorMinuto / 60;


//   while (1) {
//     passosPorLoop = passosPorSegundo / portTICK_PERIOD_MS;  // portTICK_PERIOD_MS é a duração de cada tick (geralmente 1 ms em ESP-IDF)

//   }
// }