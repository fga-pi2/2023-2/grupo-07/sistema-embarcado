#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/semphr.h>
#include <esp_system.h>
#include <driver/gpio.h>

#include "../include/botao_bms.h"

int is_charging = 0;
int is_baterry_connected = 0;
SemaphoreHandle_t mutex;

void setup(void) {
  gpio_reset_pin(PIN_CARREGAR);
  gpio_reset_pin(PIN_PARAR);

  gpio_set_direction(PIN_CARREGAR, GPIO_MODE_OUTPUT);
  gpio_set_direction(PIN_PARAR, GPIO_MODE_OUTPUT);

  gpio_set_level(PIN_CARREGAR, 0);
  gpio_set_level(PIN_PARAR, 0);
}

void ativa_modo_carregamento(void) {
  if (!is_charging) {

    printf("Começando carregamento...\n\n");
    gpio_set_level(PIN_CARREGAR, 1);
    vTaskDelay(2000 / portTICK_PERIOD_MS);
    

    gpio_set_level(PIN_CARREGAR, 0);
    
    vTaskDelay(2000 / portTICK_PERIOD_MS);
    gpio_set_level(PIN_CARREGAR, 1);
    
    vTaskDelay(100 / portTICK_PERIOD_MS);
    gpio_set_level(PIN_CARREGAR, 0);
    
    is_charging = 1;

  }
}

void para_carregamento(void) {
  if (is_charging) {

    printf("Parando carregamento...\n\n");
    gpio_set_level(PIN_PARAR, 1);
    vTaskDelay(100 / portTICK_PERIOD_MS);

    gpio_set_level(PIN_PARAR, 0);
    is_charging = 0;

  }
}

// void app_main() {
//     setup();
//     while(1) {
//       ativa_modo_carregamento();
//       vTaskDelay(15000 / portTICK_PERIOD_MS);
//       para_carregamento();
//       vTaskDelay(10000 / portTICK_PERIOD_MS);

//     }
// }
