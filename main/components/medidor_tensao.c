#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <driver/gpio.h>
#include <driver/adc.h>

#include "../include/medidor_tensao.h"

#include "../include/helpers.h"
#include "../include/botao_bms.h"

float battery_voltage_array[50];

int is_voltage_reading = 0;
int need_charging_log = 0;

int voltage_reading_counter = 0;

void setup_adc() {
    adc2_config_channel_atten(ADC2_CHANNEL_8, 0);

    gpio_reset_pin(PIN_TRANSISTOR);
    gpio_set_direction(PIN_TRANSISTOR, GPIO_MODE_OUTPUT);
}

float read_battery_voltage() {
   int row_out;
    const int adc_value = adc2_get_raw(ADC2_CHANNEL_8,ADC_WIDTH_BIT_9,&row_out);
    printf("%d\n",row_out);
    const float voltage = row_out * (3.3 / (1 << 9)); // Escala para a tensão total (3.3V) e resolução ADC (12 bits)
    return voltage;
}

void battery_measurement_task(void *pvParameters) {
  setup();
    setup_adc();

    // for(int i=0;i<50;i++){
    //     const float battery_voltage = read_battery_voltage() * (150000 + 360000) / 150000; // Ajuste do divisor de tensão 10kΩ e 2kΩ
    //     printf("Tensao da bateria: %.2f V\n", battery_voltage);
    //     battery_voltage_array[i] = battery_voltage;

    //     vTaskDelay(100 / portTICK_PERIOD_MS); // Atraso de 1 segundo
    // }x
    while(1){
            // printf("Transistor ligado\n");
            // gpio_set_level(PIN_TRANSISTOR, 0);
            // vTaskDelay(3000 / portTICK_PERIOD_MS);
            // gpio_set_level(PIN_TRANSISTOR, 1);
            // printf("Transistor desligado\n");
            // vTaskDelay(3000 / portTICK_PERIOD_MS);
        if(is_baterry_connected){
          // char body[255];
          // sprintf(body, "{\"rfid_drone\": \"%s\", \"id_base\": %s}", tagReading, ID_BASE);
          // http_request("https://monkfish-app-gyu5i.ondigitalocean.app/history/por-rfid", "POST", body);
          
          if(!is_voltage_reading && !is_charging){
            gpio_set_level(PIN_TRANSISTOR, 0);
            printf("Transistor modo carregamento.\n");

            ativa_modo_carregamento();
            vTaskDelay(10000 / portTICK_PERIOD_MS);
            // is_voltage_reading = 1;
            voltage_reading_counter = 0;
          }
          // else {
          //   if(voltage_reading_counter == 50){
          //     is_voltage_reading = 0;
          //   }
          //   para_carregamento();
          //   gpio_set_level(PIN_TRANSISTOR, 1);
          //   printf("Transistor modo leitura de tensão.\n");

          //   const float battery_voltage = read_battery_voltage() * (150000 + 360000) / 150000; // Ajuste do divisor de tensão 10kΩ e 2kΩ
          //   printf("Tensao da bateria: %.2f V\n", battery_voltage);
          //   vTaskDelay(1000 / portTICK_PERIOD_MS); // Atraso de 1 segundo
          //   voltage_reading_counter++;
          // }
        }
        vTaskDelay(1000 / portTICK_PERIOD_MS);


    }

    float moda = calcularModa(battery_voltage_array, 50);
    printf("Moda: %.2f V\n", moda);
}

// void app_main() {
//   setup();
//   setup_adc();
//   while(1){
//     gpio_set_level(PIN_TRANSISTOR, 0);
//     printf("Transistor modo carregamento.\n");

//     ativa_modo_carregamento();
//     vTaskDelay(3000 / portTICK_PERIOD_MS);
//     para_carregamento();
//     gpio_set_level(PIN_TRANSISTOR, 1);
//     printf("Transistor modo não carregamento.\n");
//     vTaskDelay(1000 / portTICK_PERIOD_MS);
//   }

//     xTaskCreate(battery_measurement_task, "battery_task", 4096, NULL, 5, NULL);
// }

// void app_main() {
//   setup();
//   setup_adc();
//   while(1) {
//     gpio_set_level(PIN_TRANSISTOR, 1);
//     printf("Transistor modo leitura de tensão.\n");

//     const float battery_voltage = read_battery_voltage() * (100000 + 360000) / 150000; // Ajuste do divisor de tensão 10kΩ e 2kΩ
//     printf("Tensao da bateria: %.2f V\n", battery_voltage);
//     vTaskDelay(1000 / portTICK_PERIOD_MS); // Atraso de 1 segundo
//     voltage_reading_counter++;
//   }
// }
