#include <stdio.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <driver/adc.h>

// #include "../include/sensor_corrente.h"

#include "../include/current_sensor.h"

#include "../include/motor.h"

// Pino ADC ao qual o ACS712 está conectado
#define ACS712_PIN ADC2_CHANNEL_3
#define ACS712_SENSIBILIDADE 0.185 // mV/A 

void current_sensor_task(void *pvParameters) {
    adc1_config_width(ADC_WIDTH_BIT_12);
    // adc1_config_channel_atten(ACS712_PIN, ADC_ATTEN_DB_11);

    while (1) {
        // Realiza uma leitura ADC
        uint32_t adc_value = adc1_get_raw(ACS712_PIN);

        // Converte o valor ADC para corrente (ajuste necessário com base nas características do ACS712)
        float voltage_reading = adc_value*(5/4095);
        float current_sensed  = (2.5 + voltage_reading)/ACS712_SENSIBILIDADE;
        // float corrente_mA = (adc_value / 4095.0) * 3300.0 / ACS712_SENSIBILIDADE;


        printf("Corrente: %.2f mA\n", current_sensed);

        // Lógica adicional com base na leitura de corrente

        vTaskDelay(pdMS_TO_TICKS(1000));  // Atraso entre leituras (1 segundo)
    }
}

// void app_main() {
//   xTaskCreate(current_sensor_task, "current_sensor_task", configMINIMAL_STACK_SIZE * 4, NULL, 5, NULL);
//   xTaskCreate(motor_task, "motor_task", 4096, NULL, 10, NULL);
// }