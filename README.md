# Sistema Embarcado

## Como executar

1. Clonar o repositório

2. Conectar a placa ESP32

3. Configurar a extensão Espressif IDF no VSCode

4. Abrir o projeto no VSCode

5. Fazer a build do projeto com ajuda da ESPIDF

6. Realizar o flash da build do projeto com ajuda da ESPIDF
